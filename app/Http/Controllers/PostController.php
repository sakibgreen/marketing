<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use DB;
use Auth;
use Session;

class PostController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct() {
        $this->middleware(['auth', 'clearance'])->except('index', 'show');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $posts = Post::where('approve','1')->orderby('id', 'desc')->paginate(8);
        
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) 
    {   
        if($request->hasFile('image_name'))
        {
            $image_name = $request->image_name->getClientOriginalName();            
            $image_size = $request->image_name->getClientSize()/1024/1024;
            $request->image_name->storeAs('public/upload',$image_name);            

            $post = new Post;
            $post->title        = $request->title;
            $post->user_id      = auth()->user()->id;
            $post->image_name   = $image_name;
            $post->image_size   = $image_size;
            $post->body         = $request->body;
            $post->address      = $request->address;
            $post->contact      = $request->contact;
            $post->save(); 

        }  
        
    //Display a successful message upon save
        return redirect()->route('posts.index')
            ->with('flash_message', 'Article,
             '. $post->title.' created');
    }


    public function approval($id) 
    {   
        $post = Post::find($id);
        if($post->approve == 1)
        {
            $post->approve = NULL;    
        }
        else
        {
            $post->approve = 1;
        }
        $post->save();

        return redirect()->back();
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post       = Post::findOrFail($id);
        return view ('posts.show', compact('post'));
    }
    
    //Manage post
    public function manage()
    {   
        if(auth()->check()){
            $posts = Post::where('user_id', auth()->user()->id)->get();
            $posts = Post::orderBy('id', 'DESC')->get(); //Latest post show first

            return view('posts.manage', compact('posts'));
        }
        else {
            return view('posts.index');
        }
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->hasFile('image_name'))
        {
            $image_name = $request->image_name->getClientOriginalName();            
            $image_size = $request->image_name->getClientSize()/1024/1024;
            $request->image_name->storeAs('public/upload',$image_name);    

            $post = Post::find($id);
            $post->update([
                'title'         =>  request('title'),
                'image_name'    =>  $image_name,
                'image_size'    =>  $image_size,
                'body'          =>  request('body'),
                'address'       =>  request('address'),
                'contact'       =>  request('contact'),

            ]);
        }

        return redirect()->route('posts.show', 
            $post->id)->with('flash_message', 
            'Article, '. $post->title.' updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        return redirect()->route('posts.manage')
            ->with('flash_message',
             'Article successfully deleted');
    }

    //trash file
    public function trashed()
    {
        $trash = DB::table('posts') //soft delete
                ->whereNotNull('deleted_at')
                ->get();

        return view('posts.trashed', compact('trash'));
    }

    public function restore($id) 
    {
        $post = Post::onlyTrashed()->findOrFail($id);
        $post->restore();
        if (!$post->trashed()) {
            // If success
            return back()->with('custom_success', 'Post has been restored.');
        } else {
            // If no success
            return back()->with('custom_errors', 'Post was not able to restore. Something went wrong.');
        }
    }

    public function forcedelete($id)
    {   
        $post = Post::withTrashed()->find($id);
        $post->forceDelete();
               
        return redirect()->route('posts.trashed')
            ->with('flash_message',
             'Article successfully deleted');
    }
}
