@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <img class="card-img-top" src="{{ asset("cover/cover.jpg")}}" width="100%" height="350" alt="Card image cap">
                    <div class="panel-heading"><h3><center>All Product</center></h3>
                    </div>
                    <div class="search-widget">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for...">
                            <span class="input-group-btn">
                                <button class="btn btn-primary" type="button">
                                    <i class="fas fa-search"></i>
                                </button>
                            </span>
                        </div><!-- /input-group -->
                    </div>
                    <div class="row marketing">
                        @foreach ($posts as $post)
                        <div class="col-lg-3">
                            <img class="card-img-top" src="{{ asset("storage/upload/".$post->image_name)}}" width="100%" height="200" alt="Card image cap">

                            <div class="panel-body">
                                <a href="{{ route('posts.show', $post->id ) }}"><b>{{ $post->title }}</b>    <br>
                                    <p class="teaser">
                                    {{str_limit($post->body, 100) }} {{-- Limit teaser to 100 characters --}}
                                    </p>
                                </a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <!--Soft Delete-->
                <div class="text-center">
                    {!! $posts->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

