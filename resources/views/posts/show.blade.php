@extends('layouts.app')

@section('title', '| View Post')

@section('content')

<div class="container">

    <h1>{{ $post->title }}</h1>

    <img src="{{ asset("storage/upload/" .$post->image_name)}}" width="500">
    <ul>
        <i class="fa fa-user"></i> {{ App\Post::user_name($post->user_id) }} 
        <i class="fa fa-clock"></i> <time> {{ $post->updated_at->diffForHumans() }}</time>
    </ul>
    
    <p class="lead">{{ $post->body }} </p>
    <p class="lead">{{ $post->address }} </p>
    <p class="lead">{{ $post->contact }} </p>
        <a href="{{ url()->previous() }}" class="btn btn-primary"><i class="fas fa-arrow-circle-left"></i> Back</a>
        
</div>

@endsection
