@extends('layouts.app')
@section('content')
	<h3><i class="fas fa-trash"></i> All Trash</h3>
	<div class="pull-right">
        	<a href="{{ route('posts.manage')}}" class="btn btn-success"><i class="fas fa-wrench"></i> Manage Post</a>
        </div>
    <table class="table table-dark">
		<thead>
    		<tr>
    			<th scope="col">Number</th>
			    <th scope="col">Title</th>
			    <th scope="col">image</th>
			    <th scope="col">Body</th>	 
				<th scope="col">Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($trash as $post)
				<tr>
					<td>{{ $loop->index+1 }}</td>
					<td>{{ $post->title }}</td>  		
	  				<td><img src="{{ asset("storage/upload/".$post->image_name)}}" width="200" height="100"></td>
					<td>{{  str_limit($post->body, 100) }}</td>
					<td><form class="form-horizontal mt-5" action="{{route('posts.restore',['id'=>$post->id])}}" method="post" enctype="multipart/form-data">
            			
            			{{csrf_field()}}
            			<button class="btn btn-info"><i class="fas fa-window-restore"></i> Restore</button>
            			</form>
					</td>
					<td><form action="{{route('posts.forcedelete',['id'=>$post->id])}}" method="post">
						{{csrf_field()}}
						<input type="hidden" name="_method" value="DELETE">
						<button class="btn btn-danger"><i class="fas fa-trash-alt"></i> Delete</button>
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection